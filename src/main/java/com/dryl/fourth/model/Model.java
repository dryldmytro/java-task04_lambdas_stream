package com.dryl.fourth.model;

import com.dryl.fourth.Main;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Model implements ModelInterface {

  private static final Logger logger = LogManager.getLogger(Main.class);

  public void getNumberUniqueWords(List<String> list) {
    long unique = list.stream().map(String::toLowerCase).distinct().count();
    logger.error("Number unique words: " + unique);
  }

  public void getSortedUniqueWords(List<String> list) {
    List<String> list1 = list.stream().map(String::toLowerCase)
        .distinct().sorted()
        .collect(Collectors.toList());
    logger.error("Sorted unique words: " + list1);
  }

  public void getWordCount(List<String> list) {
    Map<String, Long> map = list.stream().map(String::toLowerCase)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    logger.error("Word count: " + map);
  }

  public void getOccurrence(List<String> list) {
    String s = list.stream().collect(Collectors.joining());
    Map<Character, Long> map = s.chars().mapToObj(c -> (char) c)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    logger.error("Occurrence symbol: " + map);
  }
}

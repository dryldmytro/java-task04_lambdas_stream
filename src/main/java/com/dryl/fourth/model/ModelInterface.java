package com.dryl.fourth.model;

import java.util.List;

public interface ModelInterface {

  void getNumberUniqueWords(List<String> list);

  void getSortedUniqueWords(List<String> list);

  void getWordCount(List<String> list);

  void getOccurrence(List<String> list);

}

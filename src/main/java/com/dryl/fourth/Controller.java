package com.dryl.fourth;

import com.dryl.fourth.model.Model;
import java.util.List;

public class Controller {

  private Model model;
  private List<String> list;

  public Controller() {
    model = new Model();
  }

  public void setList(List<String> list) {
    this.list = list;
  }

  public void getNumberUnique() {
    model.getNumberUniqueWords(list);
  }

  public void getListUniqueWords() {
    model.getSortedUniqueWords(list);
  }

  public void getWordCount() {
    model.getWordCount(list);
  }

  public void getOccurrence() {
    model.getOccurrence(list);
  }

}

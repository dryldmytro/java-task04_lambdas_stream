package com.dryl.fourth.view;

@FunctionalInterface
public interface ViewInterface {

  void print();
}

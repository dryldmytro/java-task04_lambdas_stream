package com.dryl.fourth.view;

import com.dryl.fourth.Controller;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {

  private static final Logger logger = LogManager.getLogger(View.class);
  private static Scanner scanner = new Scanner(System.in);
  private LinkedHashMap<String, String> menu;
  private LinkedHashMap<String, ViewInterface> map;
  private Controller controller;

  public View() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    menu.put("1", "1. Get unique number.");
    menu.put("2", "2. Get list unique words");
    menu.put("3", "3. Get word count.");
    menu.put("4", "4. Get occurrence symbol.");
    menu.put("5", "5. QUIT.");
    map = new LinkedHashMap<>();
    map.put("1", this::pressed1);
    map.put("2", this::pressed2);
    map.put("3", this::pressed3);
    map.put("4", this::pressed4);
  }

  private void pressed1() {
    controller.getNumberUnique();
  }

  private void pressed2() {
    controller.getListUniqueWords();
  }

  private void pressed3() {
    controller.getWordCount();
  }

  private void pressed4() {
    controller.getOccurrence();
  }

  public void start() {
    List<String> list = new ArrayList<>();
    logger.info("Please, enter some text: ");
    String line;
    while (!(line = scanner.nextLine()).equals("")) {
      String[] lines = line.split(" ");
      Arrays.stream(lines).forEach(word -> list.add(word));
    }
    controller.setList(list);
    show();
  }

  public void outputMenu() {
    menu.forEach((k, v) -> logger.trace(v));
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      logger.info("Please, select menu.");
      keyMenu = scanner.nextLine().toUpperCase();
      try {
        map.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("5"));
  }

}

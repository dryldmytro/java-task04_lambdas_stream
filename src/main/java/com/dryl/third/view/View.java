package com.dryl.third.view;

import com.dryl.third.Controller;
import java.util.LinkedHashMap;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private final static Logger logger = LogManager.getLogger(View.class);
  private static final Scanner scn = new Scanner(System.in);
  private Controller controller;
  private LinkedHashMap<String, String> menu;
  private LinkedHashMap<String, ViewInterface> map;

  public View() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    menu.put("1", "1. Get max int.");
    menu.put("2", "2. Get count average.");
    menu.put("3", "3. Get min integer.");
    menu.put("4", "4. Get sum list.");
    menu.put("5", "5. Get numbers bigger average.");
    menu.put("6", "6. QUIT.");
    map = new LinkedHashMap<>();
    map.put("1", this::pressed1);
    map.put("2", this::pressed2);
    map.put("3", this::pressed3);
    map.put("4", this::pressed4);
    map.put("5", this::pressed5);
  }

  public void pressed1() {
    logger.error("Max integer: "+controller.getMaxInt());
  }

  public void pressed2() {
    logger.error("Average is: "+controller.getCountAverage());
  }

  public void pressed3() {
    logger.error("Min integer: "+controller.getMinInt());
  }

  public void pressed4() {
    logger.error("Sum list: "+controller.getSumList());
  }

  public void pressed5() {
    logger.error("Numbers bigger average: "+controller.getBiggerAverage());
  }

  public void outputMenu() {
    menu.forEach((k,v)-> logger.trace(v));
  }
  public void show(){
    String keyMenu;
    do {
      outputMenu();
      logger.info("Please, select menu.");
      keyMenu = scn.nextLine().toUpperCase();
      try {
        map.get(keyMenu).print();
      }catch (Exception e){}
    }while (!keyMenu.equals("6"));
  }

}
package com.dryl.third;

import com.dryl.third.view.View;

public class Main {

  public static void main(String[] args) {
    View view = new View();
    view.show();
  }
}

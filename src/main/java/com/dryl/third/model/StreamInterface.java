package com.dryl.third.model;

import java.util.List;

public interface StreamInterface {

  int getMaxInt(List<Integer> arr);

  int getCountAverage(List<Integer> arr);

  int getMinInt(List<Integer> arr);

  int getSumList(List<Integer> arr);
}

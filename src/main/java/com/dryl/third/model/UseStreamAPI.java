package com.dryl.third.model;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class UseStreamAPI implements StreamInterface {

  @Override
  public int getMaxInt(List<Integer> arr) {
    int integer = arr.stream().max(Integer::compareTo).get();
    return integer;
  }

  @Override
  public int getCountAverage(List<Integer> arr) {
    OptionalDouble od = arr.stream().sorted().mapToInt(i -> i).average();
    int integer = (int) od.getAsDouble();
    return integer;
  }

  @Override
  public int getMinInt(List<Integer> arr) {
    int integer = arr.stream().min(Integer::compareTo).get();
    return integer;
  }

  @Override
  public int getSumList(List<Integer> arr) {
    int integer = arr.stream().reduce((i, j) -> i + j).get();
    return integer;
  }

  public List<Integer> getBiggerAverage(List<Integer> arr) {
    List<Integer> integers = arr.stream().filter(x -> x > getCountAverage(arr))
        .collect(Collectors.toList());
    return integers;
  }
}

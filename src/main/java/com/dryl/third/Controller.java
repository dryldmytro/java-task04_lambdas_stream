package com.dryl.third;

import com.dryl.third.model.UseStreamAPI;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

  private final static Logger logger = LogManager.getLogger(Controller.class);
  private static final Scanner scn = new Scanner(System.in);
  private UseStreamAPI model;
  private List<Integer> list;

  public Controller() {
    model = new UseStreamAPI();
    list = getUserList();
  }

  public int getMaxInt() {
    return model.getMaxInt(list);
  }

  public int getCountAverage() {
    return model.getCountAverage(list);
  }

  public int getMinInt() {
    return model.getMinInt(list);
  }

  public int getSumList() {
    return model.getSumList(list);
  }

  public List<Integer> getBiggerAverage() {
    return model.getBiggerAverage(list);
  }

  public static List<Integer> getUserList() {
    logger.info("Please, enter the numbers.");
    String str = scn.nextLine();
    String[] strings = str.split(",");
    List<Integer> listArr = Stream.of(strings).map(z -> z.replaceAll("\\D+", ""))
        .map(s -> Integer.parseInt(s)).collect(Collectors.toList());
    return listArr;
  }
}

package com.dryl.first;

import com.dryl.first.model.Model;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

  private final static Logger logger = LogManager.getLogger(com.dryl.third.Controller.class);
  private static final Scanner scn = new Scanner(System.in);
  private int a, b, c;
  private Model model;

  public Controller() {
    model = new Model();
    getNumbers();
  }

  public void getNumbers() {
    logger.info("Please, enter the numbers.");
    a = scn.nextInt();
    b = scn.nextInt();
    c = scn.nextInt();
  }

  public int getMaxNum() {
    int max = model.getMaxCount().funcWithParam(a, b, c);
    return max;
  }

  public int getMidNum() {
    int mid = model.getMidCount().funcWithParam(a, b, c);
    return mid;
  }
}

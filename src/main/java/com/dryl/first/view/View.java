package com.dryl.first.view;

import com.dryl.first.Controller;
import java.util.LinkedHashMap;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {

  private final static Logger logger = LogManager.getLogger(com.dryl.third.view.View.class);
  private static final Scanner scn = new Scanner(System.in);
  private Controller controller;
  private LinkedHashMap<String, String> menu;
  private LinkedHashMap<String, ViewInterface> map;

  public View() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    menu.put("1", "1. Get max int.");
    menu.put("2", "2. Get count average.");
    menu.put("3", "3. QUIT.");
    map = new LinkedHashMap<>();
    map.put("1", this::pressed1);
    map.put("2", this::pressed2);
  }

  private void pressed1() {
    logger.error("Max int is: " + controller.getMaxNum());
  }

  private void pressed2() {
    logger.error("Middle int is: " + controller.getMidNum());
  }

  public void outputMenu() {
    menu.forEach((k, v) -> logger.trace(v));
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      logger.info("Please, select menu.");
      keyMenu = scn.nextLine().toUpperCase();
      try {
        map.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("3"));
  }
}

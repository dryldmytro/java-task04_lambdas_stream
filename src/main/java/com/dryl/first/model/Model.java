package com.dryl.first.model;

import java.util.Optional;
import java.util.stream.Stream;

public class Model {

  private MyInterface maxCount = (a, b, c) -> {
    Optional<Integer> opt = Stream.of(a, b, c)
        .max((first, second) -> first - second);
    return opt.get();
  };
  private MyInterface midCount = (a, b, c) -> {
    double d = Stream.of(a, b, c).sorted()
        .mapToInt(i -> i).average().getAsDouble();
    int x = (int) d;
    return x;
  };

  public MyInterface getMaxCount() {
    return maxCount;
  }

  public MyInterface getMidCount() {
    return midCount;
  }
}

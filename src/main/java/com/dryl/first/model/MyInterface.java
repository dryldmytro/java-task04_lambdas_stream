package com.dryl.first.model;

@FunctionalInterface
public interface MyInterface {

  int funcWithParam(int a, int b, int c);
}

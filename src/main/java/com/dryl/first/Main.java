package com.dryl.first;

import com.dryl.first.view.View;

public class Main {

  public static void main(String[] args) {
    View view = new View();
    view.show();
  }
}

package com.dryl.second.model.invoker;


import com.dryl.second.model.commands.Command;

public class Customer {

  private Command dessert;
  private Command drink;
  private Command salad;
  private Command soup;

  public Customer(Command dessert, Command drink, Command salad, Command soup) {
    this.dessert = dessert;
    this.drink = drink;
    this.salad = salad;
    this.soup = soup;
  }

  public String getDessert() {
    return dessert.execute();
  }

  public String writeDrink() {
    return drink.execute();
  }

  public String writeSalad() {
    return salad.execute();
  }

  public String writeSoup() {
    return soup.execute();
  }
}

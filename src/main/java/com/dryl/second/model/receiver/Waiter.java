package com.dryl.second.model.receiver;

import com.dryl.second.model.commands.Command;

public class Waiter {

  private String order;
  private Command writeSalad = () -> order;
  private Command writeDrink = new Command() {
    @Override
    public String execute() {
      return order;
    }
  };
  private Command writeSoup = writeSoup();

  public Waiter() {
  }

  public void setOrder(String order) {
    this.order = order;
  }

  public Command getWriteSalad() {
    return writeSalad;
  }

  public Command getWriteDrink() {
    return writeDrink;
  }

  private Command writeSoup() {
    return () -> order;
  }

  public String writeDessert() {
    return order;
  }

  public Command getWriteSoup() {
    return writeSoup;
  }
}

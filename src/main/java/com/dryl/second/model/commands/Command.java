package com.dryl.second.model.commands;

public interface Command {

  String execute();

}

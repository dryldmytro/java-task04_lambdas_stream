package com.dryl.second.model.commands;

import com.dryl.second.model.receiver.Waiter;

public class DessertCommand implements Command {

  private Waiter waiter;

  public DessertCommand(Waiter waiter) {
    this.waiter = waiter;
  }

  @Override
  public String execute() {
    return waiter.writeDessert();
  }
}

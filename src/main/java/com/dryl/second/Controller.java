package com.dryl.second;

import com.dryl.second.model.commands.Command;
import com.dryl.second.model.commands.DessertCommand;
import com.dryl.second.model.invoker.Customer;
import com.dryl.second.model.receiver.Waiter;
import java.util.ArrayList;
import java.util.Scanner;

public class Controller {

  private static Scanner scanner = new Scanner(System.in);
  private Waiter waiter;
  private Command salad;
  private Command dessert;
  private Command drink;
  private Command soup;
  private Customer customer;
  private ArrayList<String> arr;

  public Controller() {
    waiter = new Waiter();
    salad = waiter.getWriteSalad();
    dessert = new DessertCommand(waiter);
    soup = waiter.getWriteSoup();
    drink = waiter.getWriteDrink();
    customer = new Customer(dessert, drink, salad, soup);
    arr = new ArrayList<>();
  }

  public String getSalad() {
    start();
    return customer.writeSalad();
  }

  public String getSoup() {
    start();
    return customer.writeSoup();
  }

  public String getDessert() {
    start();
    return customer.getDessert();
  }

  public String getDrink() {
    start();
    return customer.writeDrink();
  }

  public ArrayList<String> getArr() {
    return arr;
  }

  public void start() {
    System.out.println("Enter what you want");
    String order = scanner.nextLine();
    waiter.setOrder(order);
    arr.add(order);
  }
}

package com.dryl.second.view;

import com.dryl.second.Controller;
import java.util.LinkedHashMap;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
  private final static Logger logger = LogManager.getLogger(View.class);
  private static final Scanner scn = new Scanner(System.in);
  private Controller controller;
  private LinkedHashMap<String, String> menu;
  private LinkedHashMap<String, ViewInterface> map;

  public View() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    menu.put("1", "1. Order salad. ");
    menu.put("2", "2. Order soup. ");
    menu.put("3", "3. Order dessert. ");
    menu.put("4", "4. Order drink. ");
    menu.put("5","5. Get all order.");
    menu.put("6", "6. QUIT.");
    map = new LinkedHashMap<>();
    map.put("1", this::pressed1);
    map.put("2", this::pressed2);
    map.put("3", this::pressed3);
    map.put("4", this::pressed4);
    map.put("5", this::pressed5);
    show();
  }

  private void pressed5() {
    logger.error("You order: "+ controller.getArr());
  }

  private void pressed1() {
    logger.error("You order salad: "+controller.getSalad());
  }

  private void pressed2() {
    logger.error("You order soup: "+controller.getSoup());
  }

  private void pressed3() {
    logger.error("You order dessert: "+controller.getDessert());
  }

  private void pressed4() {
    logger.error("You order drink: "+controller.getDrink());
  }
  public void outputMenu() {
    menu.forEach((k,v)-> logger.trace(v));
  }
  public void show(){
    String keyMenu;
    do {
      outputMenu();
      logger.info("Please, select menu.");
      keyMenu = scn.nextLine().toUpperCase();
      try {
        map.get(keyMenu).print();
      }catch (Exception e){}
    }while (!keyMenu.equals("6"));
  }
}
